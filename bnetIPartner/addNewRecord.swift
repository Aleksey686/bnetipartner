//
//  addNewRecord.swift
//  bnetIPartner
//
//  Created by Алексей Карпежников on 24/09/2019.
//  Copyright © 2019 Алексей Карпежников. All rights reserved.
//

//{
//    "status": 1,
//    "data":{
//        "id": "h4vcMZNbK4"
//    }
//}



import UIKit

class addNewRecord: UIViewController {

    let token = "DVPcIJc-0S-d4fgycG"
    
    @IBOutlet weak var textView: UITextView!
    
    @IBAction func saveRecord(button:UIBarButtonItem){
        if textView.isFirstResponder == true {
            self.navigationItem.rightBarButtonItem?.title = "Сохранить"
            self.view.endEditing(true)
        }else {
            // проверяем соединение с сетью
            guard isConnectedToNetwork() == true else {
                let alertController = UIAlertController( // создаем предупреждения
                       title: "Нет подключения",
                       message: "Проверте соединение с сетью",
                       preferredStyle: .alert)
                let defaultAction = UIAlertAction( // создаем кнопку
                    title: "Обновить",
                    style: .default,
                    handler: nil)
                //you can add custom actions as well
                alertController.addAction(defaultAction)
                // показываем предупреждение
                present(alertController, animated: true, completion: nil)
                return
            }
            add_entry() // после добавления записи
            textView.text = "" //очищаем поле
            self.navigationItem.rightBarButtonItem?.isEnabled = false // делаем кнопку не активной
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white // цвет view
        
        
        
        textView.delegate = self // для управления исменением цвета при наборе текста
        textView.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 20)
        textView.backgroundColor = view.backgroundColor
        textView.layer.cornerRadius = 10 // закругляем углы textView
        textView.layer.borderWidth = 2 // добавляем границу textView
        textView.layer.borderColor = UIColor.black.cgColor // красим границу в черный
        
        self.navigationItem.title = "Добавить" // создаем заголовок
        self.navigationItem.rightBarButtonItem = .init( // создаем кнопку сохранить
            title: "Сохранить",
            style: .done,
            target: self,
            action: #selector(saveRecord(button:))) // привязываем действие
        self.navigationItem.rightBarButtonItem?.isEnabled = false // сначала кнопке не работает
        
        //отслеживает появление клавиатуры
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updateTextView(notification:)),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        // отслеживает скрытие клавиатуры
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updateTextView(notification:)),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
        
    }
        
    
    //    // закрытие клавиатуры нажатием на свободную зону
    //    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    //        super.touchesBegan(touches, with: event)
    //        self.view.endEditing(true)
    //    }
    
    // скрытие клавиатуры
    @objc func updateTextView(notification: Notification){
        guard
            let userInfo = notification.userInfo as? [String:Any],
            let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            else{return}
        
        if notification.name == UIResponder.keyboardWillHideNotification{
            textView.contentInset = UIEdgeInsets.zero
        } else {
            textView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardFrame.height, right: 0)
            textView.scrollIndicatorInsets = textView.contentInset
            self.navigationItem.rightBarButtonItem?.title = "Готово"
        }
        textView.scrollRangeToVisible(textView.selectedRange)
    }
    
    func add_entry(){
        let postString = "a=add_entry&session=\(sessionTrack)&body=\(textView.text!)&name=Aleksey&email=a_karpezhnikov@mail.ru"
        var request = URLRequest(url: urlTrack!)

        request.setValue(tokenTrack, forHTTPHeaderField: "token")
        request.httpMethod = "POST"
        request.httpBody = postString.data(using: .utf8)

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            //if let response = response{print(response)}
            guard let data = data else{return}
            do{
                let coper = try JSONDecoder().decode(responseFromServer.self, from: data)
                guard coper.status == 1 else{return}
            } catch{
                print("Errors-->", error)
            }
            
        };task.resume()
    }
}

extension addNewRecord: UITextViewDelegate{
    
    // отслеживает начало набора текста (открытие клавиатуры)
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.layer.borderColor = UIColor.green.cgColor
        self.navigationItem.rightBarButtonItem?.isEnabled = true
    }
    
    // отслежавает околнчание набора текста (закрытие клавиатуры)
    func textViewDidEndEditing(_ textView: UITextView) {
        textView.layer.borderColor = UIColor.black.cgColor
        if textView.text == ""{
            self.navigationItem.rightBarButtonItem?.title = "Сохранить"
            self.view.endEditing(false)
            self.navigationItem.rightBarButtonItem?.isEnabled = false
            //textView.
        }else{
            self.navigationItem.rightBarButtonItem?.isEnabled = true
        }
    }
}
