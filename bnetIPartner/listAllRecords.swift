
//
//  listAllRecords.swift
//  bnetIPartner
//
//  Created by Алексей Карпежников on 24/09/2019.
//  Copyright © 2019 Алексей Карпежников. All rights reserved.
//
import UIKit

import Foundation
import SystemConfiguration



//структура для получения ответа после add_entry и add_session
struct responseFromServer:Codable {
    var status: Int
    var data: Dictionary<String,String>
}

//структура для получения ответа от get_entries
struct getEntriesStruct: Codable
{
    struct listData: Codable {
        struct listDataDict: Codable {
            var id: String
            var body: String
            var da: String
            var dm: String
        }
        let datadict: Array<[listDataDict]>
    }
    let data: Array<[listData]>
    let status: Int
}


func isConnectedToNetwork() -> Bool {
    var zeroAddress = sockaddr_in()
    zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
    zeroAddress.sin_family = sa_family_t(AF_INET)
    
    guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
        $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
            SCNetworkReachabilityCreateWithAddress(nil, $0)
        }
    }) else {
        return false
    }
    
    var flags: SCNetworkReachabilityFlags = []
    if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
        return false
    }
    
    let isReachable = flags.contains(.reachable)
    let needsConnection = flags.contains(.connectionRequired)
    //print(isReachable, !needsConnection)
    return (isReachable && !needsConnection)
}


var sessionTrack = ""
let tokenTrack = "DVPcIJc-0S-d4fgycG"
let urlTrack = URL(string: "https://bnet.i-partner.ru/testAPI/?a=test")




class listAllRecords: UITableViewController {
    
    // допилить условие 200 символов в строке
    let listArry = ["235234e", "135re", "135r23"]
    var listValueData = [["id": "4klJeiCKTs", "body": "Вторая запись", "da": "1442236233", "dm": "1442236233"],
    ["id": "2rRwFT9HOk", "body": "Первая запись", "da": "1442237206", "dm": "1442236206"]]
    //var listValueData: [[String:String]] = []
    let da = ["13/03/10"]
    let dm = ["14/15/10"]
    
    var statusCode: Int = 1
    var arrayRecords = [Array<Any>]() // создать массив для записей
    
    //  для перехода на страницу добавления
    @IBAction func sendPreess(button:UIBarButtonItem){
        let backItem = UIBarButtonItem()
        backItem.title = "Отмена"
        navigationItem.backBarButtonItem = backItem
        performSegue(withIdentifier: "addNewRecord11", sender: nil) //переходим к добавлению
    }
    
    @IBAction func unwintToMeMainScreen(segue: UIStoryboardSegue){
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white // цвет view
        self.navigationItem.title = "Список" //
        //self.navigationController?.navigationBar.prefersLargeTitles = true
        // создаем кнопку "Добавить"
        self.navigationItem.rightBarButtonItem = .init(
            title: "Добавить",
            style: .done,
            target: self,
            action: #selector(sendPreess(button:))) // функция перехода к экрану добавить
        //проверка соединения с сетью
        guard isConnectedToNetwork() == true else {
            let alertController = UIAlertController( // создаем предупреждения
                   title: "Нет подключения",
                   message: "Проверте соединение с сетью",
                   preferredStyle: .alert)
            let defaultAction = UIAlertAction( // создаем кнопку
                title: "Обновить",
                style: .default,
                handler: nil)
            //you can add custom actions as well
            alertController.addAction(defaultAction)
            // показываем предупреждение
            present(alertController, animated: true, completion: nil)
            return
        }
        // создаем сессию
        new_session()
        while sessionTrack == "" && statusCode == 1{} // ждем ответа от new_session
        _ = get_entries() // получаем массив объектов
        //while aaa == "", statusCode == 1 {print("b")} // ждем ответа от get_entries
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listValueData.count //количество срок на view
    }

    // заполняем таблицу
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Title", for: indexPath)
        if listValueData.count > 0{ // есть ли даннын
            cell.textLabel?.text = listValueData[indexPath.row]["body"] // добавляем запись
            if listValueData[indexPath.row]["da"] != listValueData[indexPath.row]["dm"]{ // добавляем дату
                cell.detailTextLabel?.text = "da:\(listValueData[indexPath.row]["da"]!)  dm:\(listValueData[indexPath.row]["dm"]!)"
            }else{cell.detailTextLabel?.text = "da:\(listValueData[indexPath.row]["da"]!)"}
        }
        cell.textLabel?.numberOfLines = 2 // нет огранечения на кол-во строк
        cell.textLabel?.font = cell.textLabel?.font.withSize(20)
        cell.textLabel?.lineBreakMode = .byTruncatingTail//ограничение после 2-й линии
    
        return cell
    }

    // ставим height строке
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    // передаем данные для полного просмотра
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowDetail" {
            if let indexPath = self.tableView.indexPathForSelectedRow{
                let datailVC = segue.destination as! DetailVC
                datailVC.track = listValueData[indexPath.row]["body"]!
            }
        }
    }
    
    // создание новой сессии
    func new_session(){
        
        let postString = "a=new_session&name=Aleksey&email=a_karpezhnikov@mail.ru"
        var request = URLRequest(url: urlTrack!)

        request.setValue(tokenTrack, forHTTPHeaderField: "token")
        request.httpMethod = "POST"
        request.httpBody = postString.data(using: .utf8)

        let task = URLSession.shared.dataTask(with: request) {data, _, error in
            guard let data = data else{return}
            do{
                let coper = try JSONDecoder().decode(responseFromServer.self, from: data)
                self.statusCode = coper.status
                guard coper.status == 1 else {return print(coper.data)}
                sessionTrack = coper.data["session"]! // получаем сессию
            } catch{
                print("Errors new_session-->", error)
            }
        };task.resume()
    }
    
    func get_entries() -> String {
        let postString = "a=get_entries&session=\(sessionTrack)&name=Aleksey&email=a_karpezhnikov@mail.ru"
        var request = URLRequest(url: urlTrack!)
        
        request.setValue(tokenTrack, forHTTPHeaderField: "token")
        request.httpMethod = "POST"
        request.httpBody = postString.data(using: .utf8)
        //request.httpBody = session.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let response = response{print(response)}
            guard let data = data else{return}
            do{
                print("Data--> ", data)
                let coper = try JSONDecoder().decode(getEntriesStruct.self, from: data)
                print("COPER--> ", coper)
                if coper.status == 1{
                    let aaa = coper.data
                    //self.listValueData =
                    print("AAAA-->", aaa)
                    //self.listValueData = coper.data[0] as Array<Dictionary<String,String>>
                    //listValueData = coper.data as [[String : String]]
                }
//                let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String: Any]
//                print("Json-->", json["data"]!)
//                let asdasd = json["data"] as! Array<Any>
//                print("asdasd-->",asdasd)
//                let ddd = asdasd as! Array<Array<Dictionary<String,String>>>
//                print("ddd-->", ddd)
            } catch{
                print("Errors get_entries-->", error)
            }
        };task.resume()
        return "s"
    }
  
}


