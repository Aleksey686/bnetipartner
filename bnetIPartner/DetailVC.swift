//
//  DetailVC.swift
//  bnetIPartner
//
//  Created by Алексей Карпежников on 24/09/2019.
//  Copyright © 2019 Алексей Карпежников. All rights reserved.
//

import UIKit

class DetailVC: UIViewController {

    @IBOutlet weak var textAll: UITextView!
    
    var track = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.white
        self.navigationItem.title = "Описание"
        
        textAll.text = track
        textAll.font = textAll.font?.withSize(20)
        
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
